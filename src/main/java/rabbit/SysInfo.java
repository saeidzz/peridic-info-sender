package rabbit;

import java.io.Serializable;

public class SysInfo implements Serializable {

	private static final long serialVersionUID = 8360699626785849649L;
	private static int id;
	private String userName;
	private String homePath;
	private String ipConAll;
	private String cpuCores;

	public SysInfo() {
	}

	public SysInfo(String cpuCores, String userName, String homePath, String ipConAll) {
		this.userName = userName;
		this.homePath = homePath;
		this.ipConAll = ipConAll;
		this.cpuCores = cpuCores;
	}

	public static int getId() {
		return id;
	}

	public static void setId(int id) {
		SysInfo.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getHomePath() {
		return homePath;
	}

	public void setHomePath(String homePath) {
		this.homePath = homePath;
	}

	public String getIpConAll() {
		return ipConAll;
	}

	public void setIpConAll(String ipConAll) {
		this.ipConAll = ipConAll;
	}

	public String getCpuCores() {
		return cpuCores;
	}

	public void setCpuCores(String cpuCores) {
		this.cpuCores = cpuCores;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SysInfo [userName=");
		builder.append(userName);
		builder.append(", homePath=");
		builder.append(homePath);
		builder.append(", ipConAll=");
		builder.append(ipConAll);
		builder.append(", cpuCores=");
		builder.append(cpuCores);
		builder.append("]");
		return builder.toString();
	}
}