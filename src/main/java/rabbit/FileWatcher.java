package rabbit;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

public class FileWatcher implements Runnable {
	private static final String folder = "D:\\ProgramData\\temp\\";

	@Override
	public void run() {

		Path filePath = Paths.get(folder);

		WatchService watchService;
		try {
			watchService = FileSystems.getDefault().newWatchService();

			// listen for create ,delete and modify event kinds
			filePath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		Path path;

		//
		while (true) {
			WatchKey key;
			try {
				// return signaled key, meaning events occurred on the object
				key = watchService.take();
			} catch (InterruptedException ex) {
				return;
			}

			// retrieve all the accumulated events
			for (WatchEvent<?> event : key.pollEvents()) {
				WatchEvent.Kind<?> kind = event.kind();

				System.out.println("kind " + kind.name());
				path = (Path) event.context();
				if (!path.endsWith("1")) {
					controlAllFiles();
				} else {
					FileSender fileSender = new FileSender(path.toString());

					fileSender.run();
				}
				System.out.println(path.toString());
			}
			key.reset();
		}
	}

	private void controlAllFiles() {
		// TODO Auto-generated method stub
		File folder = new File(this.folder);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
		  if (listOfFiles[i].isFile() && listOfFiles[i].getName().contains("os-run")) {
				FileSender fileSender = new FileSender(listOfFiles[i].getName());
				fileSender.run();
		  } 
		}
	}

}