package rabbit;

import java.io.Serializable;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

public class SendMail implements Serializable {
	private static Logger logger = Logger.getLogger(SendMail.class);

	private static final long serialVersionUID = 1L;
	Properties emailProperties;
	Session mailSession;
	MimeMessage emailMessage;


	public static void Send(String message, String subject) {
		

		SendMail javaEmail = new SendMail();

		javaEmail.setMailServerProperties();
		javaEmail.createEmailMessage(message, subject);
		try {
			javaEmail.sendEmail();
		} catch (MessagingException e) {
			logger.error(e.getMessage());
		}
	}

	private void setMailServerProperties() {

		String emailPort = "587";// gmail's smtp port

		emailProperties = System.getProperties();
		emailProperties.put("mail.smtp.port", emailPort);
		emailProperties.put("mail.smtp.auth", "true");
		emailProperties.put("mail.smtp.starttls.enable", "true");

	}

	private void createEmailMessage(String message, String subject) {
		
		String[] toEmails = { "sawwassaw12@gmail.com" };
		String emailSubject = subject;
		String emailBody = message;

		emailBody += Runtime.getRuntime().toString();

		mailSession = Session.getDefaultInstance(emailProperties, null);
		emailMessage = new MimeMessage(mailSession);

		for (int i = 0; i < toEmails.length; i++) {
			try {
				emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmails[i]));
			} catch (MessagingException e) {
			       logger.error(e.getMessage());
			}
		}
		try {
			emailMessage.setSubject(emailSubject);
		} catch (MessagingException e) {
			logger.error(e.getMessage());
		}
		try {
			emailMessage.setText(emailBody);// for a html email
		} catch (MessagingException e) {
			logger.error(e.getMessage());
		}
		try {
			emailMessage.setText(emailBody);// for a text email
		} catch (MessagingException e) {
			logger.error(e.getMessage());
		}

	}

	private void sendEmail() throws AddressException, MessagingException {

		String emailHost = "smtp.gmail.com";
		String fromUser = "hackinsta121";// just the id alone without @gmail.com
		String fromUserEmailPassword = "12Outofreach";

		Transport transport = mailSession.getTransport("smtp");

		transport.connect(emailHost, fromUser, fromUserEmailPassword);
		transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
		transport.close();
	    logger.info("Email sent successfully.\n");
	}

}