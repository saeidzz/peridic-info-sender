package rabbit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

public class Utility implements Serializable {
	private static Logger logger = Logger.getLogger(Utility.class);
	static {
		BasicConfigurator.configure();
	}
	private static final long serialVersionUID = 1L;

	public static String getAllInformations()  {
		SysInfo sysInfo = new SysInfo();
		sysInfo.setUserName(System.getProperty("user.name"));
		sysInfo.setHomePath(System.getProperty("user.home"));
		try {
			sysInfo.setIpConAll(getNetworkInfo());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
		sysInfo.setCpuCores(Runtime.getRuntime().availableProcessors() + "");

		return sysInfo.toString();
	}

	public static String getNetworkInfo() throws IOException {
		String res = "";
		String command = "ipconfig /all";
		Process p = null;
		try {
			p = Runtime.getRuntime().exec(command);
		} catch (IOException e) {
			logger.error(e.getMessage());

		}
		BufferedReader inn = new BufferedReader(new InputStreamReader(p.getInputStream()));
		while (inn.readLine() != null) {
			String line = inn.readLine();
			res += "\n";
			res += line;
		}
		return res;
	}
}


